package com.capgemini.survey;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.survey.entities.UserEntity;
import com.capgemini.survey.service.UserService;

//
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@TestPropertySource(properties = { "spring.profiles.active=mysql", "spring.datasource.username=root",
		"spring.datasource.password=Qwerty123" })
public class SurveyApplicationTests {
	
	@Autowired
	private UserService service;
	@PersistenceContext
	private EntityManager em;

	@Test
	public void checkUpdate() {

	}
}
