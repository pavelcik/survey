package com.capgemini.survey.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="Survey")
@Getter
@Setter
public class SurveyEntity extends AbstractEntity{
	
	@OneToOne
	private ProjectEntity project;
	@OneToOne
	private UserEntity user;
	@Enumerated(EnumType.STRING)
	private UserOpinion opinion;
	
}
