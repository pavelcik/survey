package com.capgemini.survey.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Users")
@Getter
@Setter
public class UserEntity extends AbstractEntity {

    private String name;
    private String surname;
    private String emailAddress;

    @ManyToMany(cascade=CascadeType.ALL)
    private List<UserGroupEntity> userGroup;
    @OneToOne
    private SurveyEntity survey;
}
