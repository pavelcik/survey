package com.capgemini.survey.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
@MappedSuperclass
@Getter
@Setter
public abstract class AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

}
