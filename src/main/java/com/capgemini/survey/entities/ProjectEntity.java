package com.capgemini.survey.entities;

import java.sql.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="Projects")
@Getter
@Setter
public class ProjectEntity extends AbstractEntity{

    private String projectName;
    private Date startDate;
    private Date endDate;

    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<UserGroupEntity> userGroup;

}
