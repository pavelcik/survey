package com.capgemini.survey.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.WhereJoinTable;

@Entity
@Table(name="User_Groups")
@Getter
@Setter
public class UserGroupEntity extends AbstractEntity {
    private String groupName;


    @ManyToMany(mappedBy = "userGroup",cascade=CascadeType.ALL)
    private List<UserEntity> users;

    @ManyToMany(mappedBy = "userGroup",fetch = FetchType.LAZY,cascade = CascadeType.REMOVE)
    private List<ProjectEntity> project;
}
