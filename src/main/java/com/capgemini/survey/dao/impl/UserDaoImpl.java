package com.capgemini.survey.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.capgemini.survey.dao.UserDao;
import com.capgemini.survey.entities.QUserEntity;
import com.capgemini.survey.entities.UserEntity;
import com.capgemini.survey.mapper.UserMapper;
import com.capgemini.survey.to.UserTo;
import com.querydsl.jpa.impl.JPAQuery;

@Repository
public class UserDaoImpl implements UserDao {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private UserMapper mapper;
    @Override
    public void createOne(UserEntity entity,UserTo to) {
        em.persist(mapper.map(entity,to));
    }

    @Override
    public void deleteOne(Long id) {
        UserEntity entity = em.find(UserEntity.class,id);
        em.remove(entity);
    }

    @Override
    public void updateOne(UserEntity entity,UserTo to) {
        em.merge(mapper.map(entity,to));
    }

    @Override
    public UserTo findOne(Long id) {
       return mapper.map(em.find(UserEntity.class,id));
    }

    @Override
    public List<UserTo> findAll() {
        QUserEntity user = QUserEntity.userEntity;
        JPAQuery<UserEntity> query = new JPAQuery<>(em);
        List<UserEntity> users = query.select(user).from(user).fetch();
        return mapper.map2to(users);
    }
    }

