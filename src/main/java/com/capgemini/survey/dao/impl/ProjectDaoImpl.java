package com.capgemini.survey.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.capgemini.survey.dao.ProjectDao;
import com.capgemini.survey.entities.ProjectEntity;
import com.capgemini.survey.entities.QProjectEntity;
import com.capgemini.survey.mapper.ProjectMapper;
import com.capgemini.survey.to.ProjectTo;
import com.querydsl.jpa.impl.JPAQuery;

@Repository
public class ProjectDaoImpl implements ProjectDao {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private ProjectMapper mapper;
    @Override
    public void createOne(ProjectEntity entity,ProjectTo to) {
        em.persist(mapper.map(entity,to));
    }

    @Override
    public void deleteOne(Long id) {
        ProjectEntity entity = em.find(ProjectEntity.class,id);
        em.remove(entity);
    }

    @Override
    public void updateOne(ProjectEntity entity,ProjectTo to) {
        em.merge(mapper.map(entity,to));
    }

    @Override
    public ProjectTo findOne(Long id) {
        return mapper.map(em.find(ProjectEntity.class,id));
    }

    @Override
    public List<ProjectTo> findAll() {
        QProjectEntity project = QProjectEntity.projectEntity;
        JPAQuery<ProjectEntity> query = new JPAQuery<>(em);
        return mapper.map2to(query.select(project).from(project).fetch());

    }
}