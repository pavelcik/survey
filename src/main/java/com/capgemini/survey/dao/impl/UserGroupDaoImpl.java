package com.capgemini.survey.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.capgemini.survey.dao.UserGroupDao;
import com.capgemini.survey.entities.QUserGroupEntity;
import com.capgemini.survey.entities.UserGroupEntity;
import com.capgemini.survey.mapper.UserGroupMapper;
import com.capgemini.survey.to.UserGroupTo;
import com.querydsl.jpa.impl.JPAQuery;


@Repository
public class UserGroupDaoImpl implements UserGroupDao {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private UserGroupMapper mapper;

    @Override
    public void createOne(UserGroupTo to,UserGroupEntity entity) {
        em.persist(mapper.map(entity,to));
    }

    @Override
    public void deleteOne(Long id) {
        UserGroupEntity entity = em.find(UserGroupEntity.class,id);
        em.remove(entity);
    }

    @Override
    public void updateOne(UserGroupTo to, UserGroupEntity entity) {
        em.merge(mapper.map(entity,to));
    }

    @Override
    public UserGroupTo findOne(Long id) {
       return mapper.map(em.find(UserGroupEntity.class,id));
    }

    @Override
    public List<UserGroupTo> findAll() {
    		QUserGroupEntity entity = QUserGroupEntity.userGroupEntity;
    		JPAQuery<UserGroupEntity> query = new JPAQuery<>(em);
    		return mapper.map(query.select(entity).from(entity).fetch());
    		
    }
}
