package com.capgemini.survey.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.capgemini.survey.dao.SurveyDao;
import com.capgemini.survey.entities.SurveyEntity;
@Repository
public class SurveyDaoImpl implements SurveyDao {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public void createOne(SurveyEntity entity) {
		em.persist(entity);

	}

}
