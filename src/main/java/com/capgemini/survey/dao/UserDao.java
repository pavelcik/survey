package com.capgemini.survey.dao;

import java.util.List;

import com.capgemini.survey.entities.UserEntity;
import com.capgemini.survey.to.UserTo;


public interface UserDao {
    void createOne(UserEntity entity,UserTo to);
    void deleteOne(Long id);
    void updateOne(UserEntity entity,UserTo to);
    UserTo findOne(Long id);
    List<UserTo> findAll();

}
