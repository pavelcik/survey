package com.capgemini.survey.dao;

import java.util.List;

import com.capgemini.survey.entities.UserGroupEntity;
import com.capgemini.survey.to.UserGroupTo;


public interface UserGroupDao {
    void createOne(UserGroupTo to,UserGroupEntity entity);
    void deleteOne(Long id);
    void updateOne(UserGroupTo to,UserGroupEntity entity);
    UserGroupTo findOne(Long id);
    List<UserGroupTo> findAll();

}
