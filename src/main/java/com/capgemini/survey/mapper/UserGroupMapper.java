package com.capgemini.survey.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.capgemini.survey.entities.UserEntity;
import org.springframework.stereotype.Component;

import com.capgemini.survey.entities.UserGroupEntity;
import com.capgemini.survey.to.UserGroupTo;

@Component
public class UserGroupMapper {

	public UserGroupTo map(UserGroupEntity entity) {
		UserGroupTo userGroupTo = new UserGroupTo();
		if(entity!=null) {
			userGroupTo.setId(entity.getId());
			userGroupTo.setGroupName(entity.getGroupName());
			userGroupTo.setUsersInGroup(getUsersForGroup(entity));
			userGroupTo.setId(entity.getId());
		}
		return userGroupTo;
	}

	public UserGroupEntity map(UserGroupEntity entity,UserGroupTo to) {

		if(entity!=null) {
			entity.setGroupName(to.getGroupName());
			entity.setUsers(to.getUsersInGroup());

		} else {
			entity = new UserGroupEntity();
			entity.setGroupName(to.getGroupName());
			entity.setUsers(to.getUsersInGroup());

		}
		return entity;
	}

	public List<UserEntity> getUsersForGroup(UserGroupEntity entity) {
		List<UserEntity> userEntityList = entity.getUsers();
		for(int i = 0;i<entity.getUsers().size();i++) {
			userEntityList.get(i).setUserGroup(null);
		}

		return userEntityList;
	}
	
	public List<UserGroupTo> map(List<UserGroupEntity> userGroupEntitites) {

		return userGroupEntitites.stream().map(c->map(c)).collect(Collectors.toList());
	}
}
