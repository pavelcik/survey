package com.capgemini.survey.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.capgemini.survey.entities.UserEntity;
import com.capgemini.survey.entities.UserGroupEntity;
import com.capgemini.survey.service.UserGroupService;
import com.capgemini.survey.to.UserGroupTo;
import org.springframework.stereotype.Component;

import com.capgemini.survey.entities.ProjectEntity;
import com.capgemini.survey.to.ProjectTo;

@Component
public class ProjectMapper {
	public ProjectTo map(ProjectEntity entity) {
		ProjectTo projectTo = new ProjectTo();
		if(entity!=null) {
		projectTo.setId(entity.getId());
		projectTo.setProjectName(entity.getProjectName());
		projectTo.setStartDate(entity.getStartDate());
		projectTo.setEndDate(entity.getEndDate());

	}
		return projectTo;
	}

	public ProjectEntity map(ProjectEntity entity,ProjectTo projectTo) {

		if(entity!=null) {
			entity.setEndDate(projectTo.getEndDate());
			entity.setStartDate(projectTo.getStartDate());
			entity.setProjectName(projectTo.getProjectName());
			entity.setUserGroup(getGroupsForProject(entity));
		} else {
			entity = new ProjectEntity();
			entity.setProjectName(projectTo.getProjectName());
			entity.setStartDate(projectTo.getStartDate());
			entity.setEndDate(projectTo.getEndDate());
			entity.setUserGroup(projectTo.getUserGroup());
		}
		return entity;
	}
	
	private List<UserGroupEntity> getGroupsForProject(ProjectEntity entity) {
		List<UserGroupEntity> usersInGroups = entity.getUserGroup();
		for(int i=0;i<usersInGroups.size();i++) {
			usersInGroups.get(i).setProject(null);
		}
		return usersInGroups;
	}
	
	public List<ProjectTo> map2to(List<ProjectEntity> projectEntities) {
		return projectEntities.stream().map(c->map(c)).collect(Collectors.toList());
	}


}
