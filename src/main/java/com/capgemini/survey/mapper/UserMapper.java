package com.capgemini.survey.mapper;

import com.capgemini.survey.entities.QUserEntity;
import com.capgemini.survey.entities.QUserGroupEntity;
import com.capgemini.survey.entities.UserEntity;
import com.capgemini.survey.entities.UserGroupEntity;
import com.capgemini.survey.to.UserTo;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapper {





	public UserTo map(UserEntity entity) {
		UserTo userTo = new UserTo();
		if(entity!=null) {
			userTo.setName(entity.getName());
			userTo.setSurname(entity.getSurname());
			userTo.setEmailAddress(entity.getEmailAddress());
			userTo.setId(entity.getId());

		}
		return userTo;
	}
	

	public UserEntity map(UserEntity entity,UserTo to) {
		if(entity!=null) {
			entity.setName(to.getName());
			entity.setSurname(to.getSurname());
			entity.setEmailAddress(to.getEmailAddress());
			entity.setSurvey(to.getSurveyEntity());

		} else {
			entity = new UserEntity();
			entity.setName(to.getName());
			entity.setSurname(to.getSurname());
			entity.setEmailAddress(to.getEmailAddress());
			entity.setSurvey(to.getSurveyEntity());
			entity.setUserGroup(to.getUserGroups());
	}
		return entity;
	}
//	public List<UserGroupEntity> findGroupsForUser(UserEntity entity,UserTo to) {
//		List<UserGroupEntity> groupsForUser = entity.getUserGroup();
//		for(int i=0;i<groupsForUser.size();i++) {
//			groupsForUser.get(i).setUsers(null);
//		}
//		return groupsForUser;
//	}
	public List<UserTo> map2to(List<UserEntity> userEntities) {
		return userEntities.stream().map(c->map(c)).collect(Collectors.toList());
	}
}
