package com.capgemini.survey.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.survey.entities.UserGroupEntity;
import com.capgemini.survey.service.UserGroupService;
import com.capgemini.survey.to.UserGroupTo;

@RestController
@RequestMapping("/userGroups")
public class UserGroupController {

    @Autowired
    private UserGroupService service;

    @GetMapping
    @CrossOrigin
    public List<UserGroupTo> showUserGroups() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    @CrossOrigin
    public UserGroupTo findOne(@PathVariable("id") Long id) {
        return service.findOne(id);
    }
    @PutMapping
    @CrossOrigin
    public void updateOne( UserGroupEntity entity, UserGroupTo to) {
        service.updateOne(to,entity);
    }
    @DeleteMapping("/{id}")
    @CrossOrigin
    public void deleteOne(@PathVariable("id") Long id) {
        service.deleteOne(id);
    }

    @PostMapping
    @CrossOrigin
    public void createOne(UserGroupEntity entity,UserGroupTo to) {
        service.createOne(to,entity);
    }
}
