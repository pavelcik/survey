package com.capgemini.survey.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.survey.entities.ProjectEntity;
import com.capgemini.survey.entities.SurveyEntity;
import com.capgemini.survey.service.ProjectService;
import com.capgemini.survey.service.SurveyService;
import com.capgemini.survey.to.ProjectTo;

@RestController
@RequestMapping("/projects")
public class ProjectController{

    @Autowired
    private ProjectService service;
    @Autowired
    private SurveyService surveyService;

    @GetMapping
    @CrossOrigin
    public List<ProjectTo> showProjects() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    @CrossOrigin
    public ProjectTo findOne(@PathVariable("id") Long id) {
        return service.findOne(id);
    }
    @PutMapping
    @CrossOrigin
    public void updateOne(ProjectEntity entity,ProjectTo to) {
        service.updateOne(entity,to);
    }
    @DeleteMapping("/{id}")
    @CrossOrigin
    public void deleteOne(@PathVariable("id") Long id) {
        service.deleteOne(id);
    }

    @PostMapping
    @CrossOrigin
    public void createOne(ProjectEntity entity,ProjectTo to) {
        service.createOne(entity,to);
    }
    @PostMapping("/addSurvey")
    @CrossOrigin
    public void sendSurvey(SurveyEntity entity) {
    	surveyService.createOne(entity);
    }
}
