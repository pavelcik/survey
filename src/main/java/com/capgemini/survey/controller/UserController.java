package com.capgemini.survey.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.capgemini.survey.entities.UserEntity;
import com.capgemini.survey.service.UserService;
import com.capgemini.survey.to.UserTo;
@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping
    public List<UserTo> showUsers() {
        return service.findAll();
    }

    @RequestMapping(method = RequestMethod.GET,path="/{id}")
    public UserTo findOne(@PathVariable("id") Long id) {
        return service.findOne(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void updateOne(UserEntity entity,UserTo to) {
        
        service.updateOne(entity,to);
    }

    @RequestMapping(method = RequestMethod.DELETE,path="/{id}")
    public void deleteOne(@PathVariable("id") Long id) {
        service.deleteOne(id);
    }

    @RequestMapping(method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createOne(@RequestBody UserEntity entity,UserTo to) {
        service.createOne(entity,to);
    }
}
