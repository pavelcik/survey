package com.capgemini.survey.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.survey.dao.ProjectDao;
import com.capgemini.survey.entities.ProjectEntity;
import com.capgemini.survey.to.ProjectTo;
@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {
    @Autowired
    private ProjectDao dao;


    @Override
    public void createOne(ProjectEntity entity, ProjectTo to) {
        dao.createOne(entity,to);
    }

    @Override
    public void deleteOne(Long id) {
        dao.deleteOne(id);
    }

    @Override
    public void updateOne(ProjectEntity entity, ProjectTo to) {
        dao.updateOne(entity, to);
    }

    @Override
    public ProjectTo findOne(Long id) {
       return dao.findOne(id);
    }

    @Override
    public List<ProjectTo> findAll() {
        return dao.findAll();
    }
}
