package com.capgemini.survey.service;

import java.util.List;

import com.capgemini.survey.entities.ProjectEntity;
import com.capgemini.survey.to.ProjectTo;

public interface ProjectService {

     void createOne(ProjectEntity entity,ProjectTo to);
     void deleteOne(Long id);
     void updateOne(ProjectEntity entity, ProjectTo to);
     ProjectTo findOne(Long id);
     List<ProjectTo> findAll();
}
