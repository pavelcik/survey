package com.capgemini.survey.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.survey.dao.SurveyDao;
import com.capgemini.survey.entities.SurveyEntity;
@Service
public class SurveyServiceImpl implements SurveyService {
	
	@Autowired
	private SurveyDao dao;

	@Override
	public void createOne(SurveyEntity entity) {
		dao.createOne(entity);
		
	}

}
