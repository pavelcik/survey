package com.capgemini.survey.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.survey.dao.UserDao;
import com.capgemini.survey.entities.UserEntity;
import com.capgemini.survey.to.UserTo;
@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao dao;

    @Override
    public void createOne(UserEntity entity, UserTo to) {
        dao.createOne(entity,to);
    }

    @Override
    public void deleteOne(Long id) {
        dao.deleteOne(id);
    }

    @Override
    public void updateOne(UserEntity entity, UserTo to) {
        dao.updateOne(entity,to);
    }

    @Override
    public UserTo findOne(Long id) {
        return dao.findOne(id);
    }

    @Override
    public List<UserTo> findAll() {
        return dao.findAll();
    }
}