package com.capgemini.survey.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.survey.dao.UserGroupDao;
import com.capgemini.survey.entities.UserGroupEntity;
import com.capgemini.survey.to.UserGroupTo;
@Service
@Transactional
public class UserGroupServiceImpl implements UserGroupService {
    @Override
    public void createOne(UserGroupTo to, UserGroupEntity entity) {
        dao.createOne(to, entity);
    }

    @Override
    public void deleteOne(Long id) {
        dao.deleteOne(id);
    }

    @Override
    public void updateOne(UserGroupTo to, UserGroupEntity entity) {
        dao.updateOne(to, entity);
    }

    @Override
    public UserGroupTo findOne(Long id) {
        return dao.findOne(id);
    }

    @Override
    public List<UserGroupTo> findAll() {
        return dao.findAll();
    }

    @Autowired
    private UserGroupDao dao;

}
