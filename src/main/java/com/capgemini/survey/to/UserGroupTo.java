package com.capgemini.survey.to;

import java.util.List;

import com.capgemini.survey.entities.ProjectEntity;
import com.capgemini.survey.entities.UserEntity;

import com.capgemini.survey.entities.UserGroupEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserGroupTo extends AbstractTo{
		private String groupName;
		private List<UserEntity> usersInGroup;
}
