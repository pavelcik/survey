package com.capgemini.survey.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractTo {
	private Long id;
}
