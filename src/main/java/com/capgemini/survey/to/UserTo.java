package com.capgemini.survey.to;

import java.util.List;

import com.capgemini.survey.entities.SurveyEntity;
import com.capgemini.survey.entities.UserGroupEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserTo extends AbstractTo {
	private String name;
	private String surname;
	private String emailAddress;
	private List<UserGroupEntity> userGroups;
	private SurveyEntity surveyEntity;
	

}
