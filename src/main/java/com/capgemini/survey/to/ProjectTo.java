package com.capgemini.survey.to;

import java.sql.Date;
import java.util.List;

import com.capgemini.survey.entities.UserGroupEntity;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class ProjectTo extends AbstractTo {
	  private String projectName;
	    private Date startDate;
	    private Date endDate;
	    private List<UserGroupEntity> userGroup;
}
