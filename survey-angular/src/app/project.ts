import {User} from "./user";

export class Project {
  _id: number;
  projectName: string;
  startDate: Date;
  endDate: Date;
  users: User[];
}

