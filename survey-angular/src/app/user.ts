

export class User {
  _id: any;
  name: string;
  surname: string;
  emailAddress: string;
  assignedPoll: any;
}
