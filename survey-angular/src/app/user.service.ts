import { Injectable } from '@angular/core';
import { User } from './user';
import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { MessageService} from "./message.service";
import { HttpClient, HttpHeaders} from "@angular/common/http";

import { catchError, map, tap} from 'rxjs/operators'
import 'rxjs/add/operator/map';
import {Restangular} from "ngx-restangular";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json',
    'apikey': '5a32aeb9cb25c1076c7cd9f1'})
};
@Injectable()
export class UserService {

  private userUrl = 'https://survey-a13f.restdb.io/rest/users-2';



  constructor(private http: HttpClient,
              private messageService: MessageService,
              private restangular: Restangular) { }

   getUsers(): Observable<User[]> {
    return this.restangular.all("users-2").getList();
  }

  getUser(_id: number): Observable<User> {
    return this.restangular.one("users-2",_id);
  }


  updateUser(user: User): Observable<User> {
    const id = typeof user === 'number' ? user : user._id;
    const url = `${this.userUrl}/${id}`;
    return this.http.put<User>(url,user,httpOptions);
  }
  addUser (user: User): Observable<User> {
    return this.restangular.all("users-2").post(user);
  }

  deleteUser (user: User | number): Observable<User> {
    const id = typeof user === 'number' ? user : user._id;
    const url = `${this.userUrl}/${id}`;

    return this.http.delete<User>(url, httpOptions);
  }

  }

