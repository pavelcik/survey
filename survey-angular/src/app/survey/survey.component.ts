import { Survey } from '../survey';
import { SurveyService } from '../survey.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {User} from '../user';
import { UserService } from '../user.service';

import "rxjs/add/operator/find";
import {Project} from "../project";



@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class SurveyComponent{

  name:string;
  project: Project;
  user: User;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private surveyService: SurveyService
  ) {
  }

  setGoodFeedback(survey) {
    survey.opinion="good";
    survey.submitted=true;
    survey.user = this.user;
    survey.project = this.project;

    this.sendSurvey(survey);
  }
  setNeutralFeedback(survey) {
    survey.opinion="neutral";
    survey.submitted=true;
    this.sendSurvey(survey);
  }
  setBadFeedback(survey) {
    survey.opinion="bad";
    survey.submitted=true;
    this.sendSurvey(survey);
  }

  sendSurvey(survey) {
    this.surveyService.addSurvey(survey);
  }






//  getProject(): void {
//    const id = +this.route.snapshot.paramMap.get('id');
//    this.projectService.getProject(id)
//      .subscribe(project=>this.project=project);
//  }

  // getUserByName(name) {
  //   return this.userService.getUsers().find(x => {
  //     return x.name === name;
  //   });
  // }
  // save(survey) {
  //
  //  survey.user = this.getUserByName(this.name).id;
  //
  //   this.surveyService.addSurvey(this.survey)
  //     .subscribe();
  // }

}
