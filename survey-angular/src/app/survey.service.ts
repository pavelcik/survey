import { Injectable } from '@angular/core';
import { Project } from './project';
import  { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { MessageService} from "./message.service";
import { HttpClient, HttpHeaders} from "@angular/common/http";
import { catchError, map, tap} from 'rxjs/operators'
import { Survey } from './survey';
import {Restangular} from "ngx-restangular";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json',
    'apikey': '5a32aeb9cb25c1076c7cd9f1'})
};
@Injectable()
export class SurveyService {
  private surveyUrl = 'https://survey-a13f.restdb.io/rest/survey';
  private url;
  constructor(private http: HttpClient,
  private messageService: MessageService,
              private restangular: Restangular) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead

      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add('ProjectService: ' + message);
  }

  addSurvey (survey: Survey): Observable<Survey> {
    const base = 'https://localhost:4200/survey/'
    survey.url = base.concat(survey._id);
    return this.restangular.all("survey").post(survey);
  }

}
