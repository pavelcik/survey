import { Component, OnInit } from '@angular/core';
import {Project} from "../project";
import {ProjectService} from "../project.service";

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  projects: Project[] = [];
  constructor(private projectService: ProjectService) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void {
    this.projectService.getProjects()
      .subscribe(projects=>this.projects = projects.slice(0,4));
  }

}
