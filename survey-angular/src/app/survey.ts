import {User} from "./user";
import {Project} from "./project";

export class Survey {
  _id:any;
  opinion: string;
  project: Project;
  user: User;
  url:string;
  submitted: boolean;
  baseUrl: string = "https://survey-a13f.restdb.io/rest/survey/";

}
