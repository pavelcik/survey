import { Injectable } from '@angular/core';
import {MessageService} from "./message.service";
import {of} from "rxjs/observable/of";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {catchError, tap} from "rxjs/operators";
import {UserGroup} from "./userGroup";
import {Restangular} from "ngx-restangular";
import {User} from "./user";
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json',
  'apikey' : '5a32aeb9cb25c1076c7cd9f1'})
};
@Injectable()
export class UserGroupService {

  private userGroupUrl = 'https://survey-a13f.restdb.io/rest/usergroup';



  constructor(private http: HttpClient,
              private messageService: MessageService,
              private restangular: Restangular) { }

  getUserGroups(): Observable<UserGroup[]> {
    return this.restangular.all("usergroup").getList();
  }

  getUserGroup(_id: number): Observable<UserGroup> {
    return this.restangular.one("usergroup",_id);
  }


  updateUserGroup(userGroup: UserGroup): Observable<any> {
    const id = typeof userGroup === 'number' ? userGroup : userGroup._id;
    const url = `${this.userGroupUrl}/${id}`;
    return this.http.put<UserGroup>(url,userGroup,httpOptions);
  }
  addUserGroup (userGroup: UserGroup): Observable<UserGroup> {
    return this.restangular.all("usergroup").post(userGroup);
  }

  deleteUserGroup (userGroup: UserGroup | number): Observable<UserGroup> {
    const id = typeof userGroup === 'number' ? userGroup : userGroup._id;
    const url = `${this.userGroupUrl}/${id}`;

    return this.http.delete<UserGroup>(url, httpOptions);
  }

  // getUsersForUserGroup(){
  //   return.this.restangular.
  // }
}
