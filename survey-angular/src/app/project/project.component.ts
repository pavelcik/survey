import { Component, OnInit, Input } from '@angular/core';
import { Project } from "../project";
import { ProjectService } from "../project.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Location} from "@angular/common";
import {ActivatedRoute,Router} from "@angular/router";
import {FormBuilder,FormGroup,Validators} from "@angular/forms";
import {User} from "../user";
import {UserService} from "../user.service";
import {IDropdownItem} from "ng2-dropdown-multiselect/dist";

//const generateUrl = require('generate-url');
@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})

export class ProjectComponent implements OnInit {

  projects: Project[];
  users: User[];
  @Input() project: Project;
    order: string = 'name';
  reverse: boolean = false;
  rForm: FormGroup;
  surveyForm: FormGroup;
  projectName: string;

  userLink: string;
  titleAlert: string = 'This field is required';
  startDate: Date;
  endDate: Date;


  constructor(private projectService: ProjectService,
              private route: ActivatedRoute,
              private modalService: NgbModal,
              private location: Location,
            private userService: UserService,
            private router: Router,
              private fb: FormBuilder) {    this.rForm = fb.group({
    'projectName': [null, Validators.required],
    'startDate': [null, Validators.required],
    'endDate': [null, Validators.required]
  },{validator: this.dateLessThan('startDate','endDate')}),
    this.surveyForm = fb.group({
      'user': [null, Validators.required]
    })
  }
  ngOnInit() {
    this.getProjects();

  }
  dateLessThan(startDate,endDate) {
    return (group: FormGroup): {[key: string]: any} => {
      let f = group.controls[startDate];
      let t = group.controls[endDate];
      if (f.value > t.value) {
        return {
          dates: "Date from should be less than Date to"
        };
      }
      return {};
    }
  }
  addForm(project: Project) {
    let date = new Date();
    this.projectName=project.projectName;
    this.startDate=project.startDate;
    this.endDate=project.endDate;
    this.projectService.addProject(project).subscribe(project=>this.projects.push(project))
  }

  getProjects(): void {
    this.projectService.getProjects()
      .subscribe(projects=>this.projects = projects);
  }

  add(projectName: string,startDate: Date,endDate: Date) {
    if (!projectName||!startDate||!endDate) { return; }
    this.projectService.addProject(this.project)
      .subscribe(project => {
        this.projects.push(project);
      });
  }

  delete(project: Project): void {
    this.projects = this.projects.filter(h => h !== project);
    this.projectService.deleteProject(project).subscribe();
  }

  open(project,modal) {
    this.project = project;
    this.modalService.open(modal);
  }
  openAdd(modal){
    this.modalService.open(modal);
  }
  openSurvey(project,modal){
    this.project=project;
    this.modalService.open(modal);
  }

  goBack(): void {
    this.location.back();
  }
  save(): void {
  this.projectService.updateProject(this.project)
.subscribe(() => this.goBack());
}

    setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }

  sendSurvey(user,project) {
    console.log(user.name);
    console.log(project.projectName);


  }



}
