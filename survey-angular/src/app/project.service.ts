import { Injectable } from '@angular/core';
import { Project } from './project';
import  { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { MessageService} from "./message.service";
import { HttpClient, HttpHeaders} from "@angular/common/http";
import { catchError, map, tap} from 'rxjs/operators'
import { Survey } from './survey';
import {SurveyService} from './survey.service';
import {Restangular} from "ngx-restangular";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json',
    'apikey': '5a32aeb9cb25c1076c7cd9f1'})
};
@Injectable()
export class ProjectService {

  private userUrl = 'https://survey-a13f.restdb.io/rest/projects';



  constructor(private http: HttpClient,
              private messageService: MessageService,
            private surveyService: SurveyService,
              private restangular: Restangular) { }

  getProjects(): Observable<Project[]> {
    return this.restangular.all("projects").getList();
  }

  getProject(_id: number): Observable<Project> {
    // const url = `${this.userUrl}/${id}`;
    // return this.http.get<Project>(url);
    return this.restangular.one("projects",_id);
  }


  updateProject(project: Project): Observable<any> {
    const id = typeof project === 'number' ? project : project._id;
    const url = `${this.userUrl}/${id}`;
    return this.http.put<Project>(url,project,httpOptions);
  }
  addProject (project: Project): Observable<Project> {

    return this.restangular.all("projects").post(project);
  }

  deleteProject (project: Project | number): Observable<Project> {
    const id = typeof project === 'number' ? project : project._id;
    const url = `${this.userUrl}/${id}`;
    return this.http.delete<Project>(url,httpOptions)
  }

  sendSurvey(survey: Survey) {
    this.surveyService.addSurvey(survey);
  }
}
