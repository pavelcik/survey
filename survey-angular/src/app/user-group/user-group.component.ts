import {Component, Input, OnInit} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import {UserGroupService} from "../user-group.service";
import {UserGroup} from "../userGroup";
import {User} from "../user";
import {FormBuilder,FormGroup,Validators} from "@angular/forms";


@Component({
  selector: 'app-user-group',
  templateUrl: './user-group.component.html',
  styleUrls: ['./user-group.component.css']
})
export class UserGroupComponent implements OnInit {
  userGroups: UserGroup[];
  usersInGroup: User[];
  @Input() userGroup: UserGroup;
    order: string = 'name';
  reverse: boolean = false;
  groupForm: FormGroup;
  groupName: string;
  titleAlert: string = "This value is required";

  constructor(private userGroupService: UserGroupService,
              private route: ActivatedRoute,
              private modalService: NgbModal,
              private location: Location,private fb: FormBuilder) {    this.groupForm = fb.group({
    'groupName': [null, Validators.required]

    })
  }
  ngOnInit() {
    this.getUserGroups();
  }

  getUserGroups(): void {
    this.userGroupService.getUserGroups()
      .subscribe(userGroups => this.userGroups = userGroups);
  }

  addForm(userGroup: UserGroup) {
    this.groupName=userGroup.groupName;
    this.userGroupService.addUserGroup(userGroup).subscribe(userGroup=>this.userGroups.push(userGroup));
  }

  delete(userGroup: UserGroup): void {
    this.userGroups = this.userGroups.filter(h => h !== userGroup);
    this.userGroupService.deleteUserGroup(userGroup).subscribe();
  }

  open(userGroup, modal) {
    this.userGroup = userGroup;
    this.modalService.open(modal);
  }

  openAdd(modal) {
    this.modalService.open(modal);
  }
  openUsers(usersInGroup,modal) {
    this.usersInGroup=usersInGroup;
    this.modalService.open(modal);
  }

  goBack(): void {
    this.location.go('https://survey-a13f.restdb.io/rest/usergroup');
  }

  save(): void {
    this.userGroupService.updateUserGroup(this.userGroup)
      .subscribe(() => this.goBack());
  }



    setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }
  add() {
    this.userGroupService.addUserGroup(this.userGroup).subscribe(() => this.goBack());
  }

}
