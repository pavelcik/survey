import { TestBed, inject } from '@angular/core/testing';

import { UserService } from './user.service';
import {RESTANGULAR} from "ngx-restangular/dist/esm/src/ngx-restangular.config";
import {User} from "./user";

describe('UserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserService,RESTANGULAR]
    });
  });

  it('should be created', inject([UserService,RESTANGULAR], (service: UserService,Restangular restAngular) => {
    expect(restAngular.all.typeOf).toBe(User);
  }));
});
